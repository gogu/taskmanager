/*Variabile utilizate pentru a seta in controlul datepicker zilele ce nu sunt valide pentru selectare, respectiv datele anterioare datei curente, se utilizeaza la adaugarea unui task nou*/
var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

/*Functia permite trimiterea formularului de cautare daca sunt indeplinite criteriile, campul q nu este gol, daca a fost selectat un interval de zile se valideaza ca datele sa fie corecte respectiv start_date < end_date*/
function Search()
{
	var qVal   = $('#q').val();
	var dStart = $('#start_date').val();
	var dEnd   = $('#end_date').val();
	var fromDate;
	var toDate;
	
	if(qVal == '' && dStart == '' && dEnd == '')
		return false;
	
	if(dStart != '' &&  dEnd != '')
	{
		if(dStart == dEnd)
		{
			alert('Cele doua date nu pot fi identice!');
			$('#q').focus();
			return false;
		}
		
		fromDate = new Date(FormatDate(dStart)).getTime();
		toDate   = new Date(FormatDate(dEnd)).getTime();
		
		if(fromDate > toDate)
		{
			alert('Data de la care se face cautarea nu este corecta!');
			return false;
		}
	}
	
	if(qVal != '' || (dStart != '' &&  dEnd != ''))
		return true;
	
	return false;
}

/*
	Functie utilizata pentru resetarea campurilor din formularul de search
*/
function ResetFields()
{
	$('#q, #start_date, #end_date').val('');
}

/*
	Functia face conversia din formatul dd-mm-yyyy in yyyy/mm/dd pentru a se putea utiliza cu obiectul Date
*/

function FormatDate(inDate)
{
	var outDate = inDate.split('-');
	return outDate[2]+'/'+outDate[1]+'/'+outDate[0];
}

/*
	Functia utilizata pentru salvarea datelor
*/
function Save()
{
	var params = [];
	var surl = '?action=save';
	
	$('#addTaskForm input').each(function(){ params.push($(this).attr('id')+'='+$(this).val()); });
	
	$.ajax({type: "POST", url: surl, data: params.join('&'), dataType:'json',processDataBoolean: false}).done(function( msg ){
		
		if(msg.iserror == 0)
		{
			$('#addTaskForm').modal('hide');
			$('#addTaskForm input').val('');
		}
		else
		{
			if($('#tdesc').val() == '')
				$('#tdesc').focus();
			else
				$('#tdate').focus();
		}
		alert(msg.message);
		location.href = SELF_LOCATION;
	});
}

/*
	Functia utilizata pentru stergerea taskurilor
*/

function Delete(taskId)
{
	var surl = '?action=delete';
	
	if(parseInt(taskId) != 'NaN')
	{
		surl += '&id='+taskId;
		
		if(confirm("Sunteti sigur ca vreti sa stergeti taskul?") == true) 
		{
			
			$.ajax({type: "GET", url: surl, dataType:'json',processDataBoolean: false}).done(function( msg ){
				alert(msg.message);
				location.reload();
			});
		}
	}
}

/*
	Functia utilizata pentru setarea statusului taskurilor
*/

function Status(taskId)
{
	var surl = '?action=status';
	
	if(parseInt(taskId) != 'NaN')
	{
		surl += '&id='+taskId;
		
		$.ajax({type: "GET", url: surl, dataType:'json',processDataBoolean: false}).done(function( msg ){
			alert(msg.message);
			location.reload();
		});
	}
}

$(document).ready(function()
{
	/*Se adauga eventul onClick butonului cu id-ul taskSaveBtn */
	$('#taskSaveBtn').bind({click: function() {Save();}});
	/*Se adauga eventul onClick pe butoanele ce au clasa btnInchide - se seteaza statusul taskului ca fiind inchis */
	$('#tasksListTable .btnInchide').bind({click: function() {Status($(this).attr('rel'))}});
	/*Se adauga eventul onClick pe butoanele ce au clasa btnInchide - se seteaza taskul ca fiind sters */
	$('#tasksListTable .btnSterge').bind({click: function() {Delete($(this).attr('rel'))}});
});