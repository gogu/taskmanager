<?php
if(isset($tasksData) && isset($tasksData['items']) && count($tasksData['items']) && !$Tasks->tisdel):
?>
<form class="panel" id="searchpanel" method="get" onsubmit="return Search()">
	<div class="panel-heading">Cauta dupa</div>
	<fieldset>
		<p><label>Descriere:</label> <input type="text" name="q" id="q" placeholder="" value="<?php echo (isset($Tasks->search_query) && $Tasks->search_query)?$Tasks->search_query:'';?>"></p>
		<p><label>De la:</label> <input  type="text" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" readonly="readonly" value="<?php echo (isset($Tasks->start_date) && $Tasks->start_date)?date('d-m-Y',strtotime($Tasks->start_date)):'';?>"></p>
		<p><label>Pana la:</label> <input  type="text" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" readonly="readonly" value="<?php echo (isset($Tasks->end_date) && $Tasks->end_date)?date('d-m-Y',strtotime($Tasks->end_date)):'';?>"></p>
		<p class="buttonContainer text-right">
			<button type="button" class="btn" onclick="ResetFields();">Reseteaza campurile</button>
			<button type="submit" class="btn btn-primary" style="margin-right:45px">Cauta</button>
		</p>
		<script type="text/javascript">
			var tStartDate = $('#start_date').datepicker({
							onRender: function(date) 
							{
								return date.valueOf() > now.valueOf() ? 'disabled' : '';
							}
							}).on('changeDate', function(ev) {
								tStartDate.hide();
							}).data('datepicker');
			
			var tEndDate = $('#end_date').datepicker({
							onRender: function(date) 
							{
								return date.valueOf() > now.valueOf() ? 'disabled' : '';
							}
							}).on('changeDate', function(ev) {
								tEndDate.hide();
							}).data('datepicker');
		</script>
	</fieldset>
</form>
<?php
endif;
?>