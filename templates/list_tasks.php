<?php
if(isset($tasksData) && isset($tasksData['items']) && count($tasksData['items'])):
?>
	<?php if(!$Tasks->tisdel): ?>
		<table class="table data_table well" id="tasksListTable">
			<tr>
				<th class="span1">ID</th>
				<th >Descriere</th>
				<th class="text-center">Data</th>
				<th class="text-center">Inchis</th>
				<th class="text-center">Actiuni</th>
			</tr>
	<?php
		$table_out = null;
		foreach($tasksData['items'] as $task)
		{
			$table_out .= '<tr>
							<td class="span1">'.$task['id'].'</td>
							<td>'.$task['tdesc'].'</td>
							<td class="text-center">'.$task['tdate'].'</td>
							<td class="text-center"><span class="'.$task['tstatus_class'].'">'.$task['tstatusstr'].'</span></td>
							<td class="text-right span4">';
			if(!$task['tstatus'])
				$table_out .= '<a class="btn btn-small btnInchide" title="Terminat" href="#" rel="'.$task['id'].'"><i class="icon-ok"></i> Inchide</a> ';
				
			$table_out .= '<a class="btn btn-small btnSterge" title="Delete" href="#"  rel="'.$task['id'].'"><i class="icon-trash"></i> Sterge</a>
						</td>
					</tr>';
		}
		echo $table_out;
	?>
	<?php else: ?>
		<table class="table data_table well">
			<tr>
				<th>Descriere</th>
				<th class="text-center">Inchis</th>
				<th class="text-center">Data stergerii</th>
			</tr>
		<?php
			foreach($tasksData['items'] as $task)
				echo	 '<tr>
							<td>'.$task['tdesc'].'</td>
							<td class="text-center"><span class="'.$task['tstatus_class'].'">'.$task['tstatusstr'].'</span></td>
							<td class="text-center">'.$task['tdelday'].'</td>
						 </tr>';
		?>
	<?php
	endif;
	?>
	</table>
<?php
else:
?>
	<div class="alert alert-error panel">  
	  <div class="panel-heading">Eroare</div>
	  Momentan nu aveti taskuri <?= (!$Tasks->tisdel)?'adaugate':'sterse';?>.
	</div>
<?php
endif;
?>