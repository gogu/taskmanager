<?php
if(isset($tasksData) && isset($tasksData['pages']) && $tasksData['pages']['totalpages'] > 1):
	$base_location = (!$Tasks->tisdel)?SELF_LOCATION:SELF_LOCATION.'?action=viewlog';
	$add_char	   = (!$Tasks->tisdel)?'?':'&';
	$q_string	   = null;
	
	if(isset($_GET['pn']) && $_GET['pn'])
		unset($_GET['pn']);
	
	if(isset($_GET['action']) && $_GET['action'])
		unset($_GET['action']);
		
	$q_string 	   = http_build_query($_GET);
	$q_string	   = ($q_string)?'&'.$q_string:null;
	
	
?>
<div class="pagination">
  <ul>
<?php
	if($tasksData['pages']['prevpage'])
	{
		if($tasksData['pages']['prevpage'] == 1)
		{
			$tmpq_string = null;
			
			if($q_string)
				$tmpq_string = '?'.substr($q_string,1, strlen($q_string));
				
			$url = $base_location.$tmpq_string;
		}
		else
			$url = $base_location.$add_char.'pn='.$tasksData['pages']['prevpage'].$q_string;
		
		echo '<li><a href="'.$url.'">&laquo;</a></li>';
	}
	
	for($i = 1; $i <= $tasksData['pages']['totalpages']; $i++)
	{
		if($i == 1)
		{
			$tmpq_string  = null;
			
			if($q_string)
				$tmpq_string = '?'.substr($q_string,1, strlen($q_string));
				
			$url = $base_location.$tmpq_string;
		}
		else
			$url = $base_location.$add_char.'pn='.$i.$q_string;
			
		//$url = ($i == 1)?$base_location.$q_string:$base_location.$add_char.'pn='.$i.$q_string;
		echo ($i ==  $tasksData['pages']['curpage'])?'<li class="active disabledTab"><a href="'.$url.'">'.$i.'</a></li>':'<li><a href="'.$url.'">'.$i.'</a></li>';
	}
	
	if($tasksData['pages']['next'])
		echo '<li><a href="'.$base_location.$add_char.'pn='.$tasksData['pages']['next'].$q_string.'">&raquo;</a></li>';
?>	
  </ul>
</div>
<?php
endif;
?>