<div class="modal fade" id="addTaskForm" tabindex="-1" role="dialog" aria-labelledby="addTaskForm" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Adauga task</h4>
	  </div>
	  <div class="modal-body">
		 <fieldset>
			<p>
				<label class="required">Descriere task:</label> <input type="text" name="tdesc" id="tdesc" placeholder="Descriere task">
			</p>
			<p>
				<label class="required">Data inceperii:</label> <input  type="text" id="tdate" name="tdate" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" readonly="readonly">
			</p>
			<script type="text/javascript">
				var tDateDp = $('#tdate').datepicker({
					onRender: function(date) 
					{
						return date.valueOf() < now.valueOf() ? 'disabled' : '';
					}
					}).on('changeDate', function(ev) {
						tDateDp.hide();
					}).data('datepicker');
			</script>
		 </fieldset>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
		<button type="button" class="btn btn-primary" id="taskSaveBtn">Salveaza</button>
	  </div>
	</div>
  </div>
</div>