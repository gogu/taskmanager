<?php
//Se verifica existenta fisierului de configurare
if(!file_exists('conf.php') || !filesize('conf.php'))
	die('Fisierul de configurare nu a fost gasit sau este gol!');

//Se include fisierul de configurare
include('conf.php');

//Se include fisierul ce contine clasa DB
require(ABSPATH.'code'.DS.'DB_class.php');
//Se include fisierul ce contine clasa DB
require(ABSPATH.'code'.DS.'Task_class.php');

//Se instantiaza clasa Database
$DB_connect = new Database();

if(!$DB_connect)
	die('Conectarea la baza de date a esuat!');
	
//Se instantiaza clasa Tasks
$Tasks = new Tasks($DB_connect);


if(isset($_GET['action']) && $_GET['action'])
{
	if(in_array($_GET['action'], $Tasks->allowed_actions))
	{
		switch($_GET['action'])
		{
			case 'save':  $Tasks->Save(); exit;
			case 'status': $Tasks->SetStatus(); exit;
			case 'delete': $Tasks->Delete(); exit;
			case 'viewlog': $tasksData = $Tasks->ViewLog();break;
		}
	}
	else
		die('Aceasta actiune nu este permisa!');
}
else
	$tasksData = $Tasks->GetTasks();
?>
<!DOCTYPE html>
<head>
<title>Task Manager</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/datepicker.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript">
	var SELF_LOCATION = '<?= SELF_LOCATION; ?>';
</script>
</head>
<body>
<!--TOP MENU START-->
<div class="container">
	<div class="navbar" style="margin-top:20px">
		  <div class="navbar-inner">
				<div class="container">
					  <ul class="nav pull-right">
							<li><a href="<?= SELF_LOCATION;?>"><i class="icon-th-large"></i> Vezi taskuri</a></li>
							<li><a href="<?= SELF_LOCATION.'?action=viewlog';?>"><i class="icon-th-large icon-eye-open"></i> Log taskuri sterse</a></li>
							<li><a href="#" data-toggle="modal" data-target="#addTaskForm"><i class="icon-th-large icon-plus"></i> Adauga task</a></li>
					  </ul>
				</div>
		  </div>
	</div>
</div>
<!--TOP MENU END-->
<div class="container">
	<!-- CONTENT ZONE START -->
	<div class="row-fluid">
		<!-- FILTERS START -->
		<?php include(ABSPATH.'templates'.DS.'search_box.php');?>
		<!-- FILTERS END -->
		<!-- TASKS LIST START -->
		<?php include(ABSPATH.'templates'.DS.'list_tasks.php');?>
		<!-- TASKS LIST END -->
		<!-- PAGINATION START -->
		<?php include(ABSPATH.'templates'.DS.'pagination.php');?>
		<!-- PAGINATION END -->
	</div>
	<!-- MODAL WINDOW START -->
	<?php include(ABSPATH.'templates'.DS.'modal_box.php');?>
	<!-- MODAL WINDOW END -->
    <!--CONTENT ZONE END-->
	<hr>
	<!--FOOTER START-->
	<div class="footer navbar navbar-fixed-bottom" style="text-align:center;margin:0 auto"><p>&copy; <?= date('Y'); ?></p></div>
	<!--FOOTER END-->
</div>
</body>
</html>