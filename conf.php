<?php
// set true if production environment else false for development
define ('IS_ENV_PRODUCTION', false);

// configure error reporting options
error_reporting(E_ALL);
ini_set('display_errors', !IS_ENV_PRODUCTION);

// force magic_quotes_runtime turn off
ini_set('magic_quotes_runtime', 'off');

//set as constant directory separator (differ from Linux vs. Windows)
define('DS', DIRECTORY_SEPARATOR);

//set as constant current working directory
define('ABSPATH',dirname(__FILE__).DS);

//set as constant current www location
define('SELF_LOCATION',rtrim( dirname( $_SERVER['PHP_SELF'] ), '/\\' ) . '/');

// configure database options
define('DBhost','localhost');
define('DBuser','root');
define('DBpass','');
define('DBname','taskmanager');
define('DBprefix','');
?>