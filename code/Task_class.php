<?php
/**
 * Tasks class
 *  @author     Alexandru Tertisco <alex@diffstudios.com>
 *  @version    1.0.0  (13.05.2014)
 *  @copyright  (c) 2014 Alexandru Tertisco
 *  @package    TaskManager
 *
 **/
 
class Tasks
{
	/**
	* Holds task's id.
	*
	* @var int
	*/
	var $id;
	/**
	* Holds filter start date.
	*
	* @var string 
	*/
	var $start_date;
	/**
	* Holds filter end date.
	*
	* @var string 
	*/
	var $end_date;
	/**
	* Holds current page number.
	*
	* @var int 
	*/
	var $pn = 1;
	/**
	* Holds number of listed items / page
	*
	* @var int
	*/
	var $items_per_page = 10;
	
	/**
	* Holds flag if is log view action has been requested
	*
	* @var int 
	*/
	var $tisdel = 0;
	
	/**
	* Contains search string
	*
	* @var string 
	*/
	var $search_query = null;
	
	/**
	* Database object
	*
	* @var object
	*/
	protected $db;
	
	/**
	* Allowed actions to be requested
	*
	* @var array
	*/
	var $allowed_actions = array('save', 'viewlog', 'delete', 'status');
	
	/**
	* Constructor. Initializes a database connection and selects our database.
	*
	* @param  object  Database object
	*/  
	function Tasks($dbobj = null)
	{
		if($dbobj)
			$this->db = $dbobj;
		else
			die('Database object is null.');
		
		if(isset($_GET['action']) && $_GET['action'] == 'viewlog')
			$this->tisdel = 1;
			
		if(isset($_GET['pn']) && $_GET['pn'])
			$this->pn = abs(intval($_GET['pn']));
			
		if(isset($_GET['id']) && $_GET['id'])
			$this->id = abs(intval($_GET['id']));
			
		if(isset($_GET['start_date']) && $this->start_date = self::DoDBDate($_GET['start_date']))
			$this->start_date = $this->db->quote_smart($this->start_date);
		
		if(isset($_GET['end_date']) && $this->end_date = self::DoDBDate($_GET['end_date']))
			$this->end_date = $this->db->quote_smart($this->end_date);
			
		if(isset($_GET['q']) && $this->search_query = $this->db->CleanString($_GET['q']))
			$this->search_query = $this->db->quote_smart($this->search_query);
	}
	
	/**
	 * Validates and convert dates from dd-mm-yyyy to MySQL format yyyy-mm-dd
	 *
	 * @param  string  raw formated date received 
	 * @return mixed
	 */
	function DoDBDate($raw_date)
	{
		if($raw_date = $this->db->CleanString($raw_date))
		{
			$raw_date = explode('-', $raw_date);
			
			if(count($raw_date) == 3)
				return $raw_date[2].'-'.$raw_date[1].'-'.$raw_date[0];
		}
		return false;
	}
	
	/**
	 * Executes sql query in order to obtain tasks, also used to search for tasks
	 *
	 * @return mixed
	 */
	function GetTasks()
	{
		$query = null;
		
		if($this->search_query)
			$query = "WHERE tdesc LIKE '%{$this->search_query}%' ";

		if($this->start_date && $this->end_date)
		{
			$query .= ($this->search_query)?'AND ':'WHERE ';
			$query .= "tdate BETWEEN '{$this->start_date}' AND '{$this->end_date}' ";
		}
		
		$query = $this->db->do_query("SELECT SQL_CALC_FOUND_ROWS * FROM tasks
									  $query
									  ORDER BY tdate DESC, id DESC LIMIT ".self::getCurrentIndex().",".$this->items_per_page);
										  
		if($query && mysql_num_rows($query))
		{
			$_query = $this->db->do_query("SELECT FOUND_ROWS() as total",true);
			$items_list['pages']['curpage']    = $this->pn;
			$items_list['pages']['totalpages'] =  ceil($_query['total']/$this->items_per_page);
			$items_list['pages']['prevpage']   = ($this->pn > 1)?($this->pn -1):null;
			$items_list['pages']['next']   	   = ($this->pn != $items_list['pages']['totalpages'])?($this->pn +1):null;
			
			while($item_data = mysql_fetch_row($query, MYSQL_ASSOC))
			{
				$item_data['tstatus_class'] = ($item_data['tstatus'])?'success':'info';
				$item_data['tstatusstr']    = ($item_data['tstatus'])?'Da':'Nu';
				$item_data['tdate']    		= date('d-m-Y',strtotime($item_data['tdate']));
				$items_list['items'][] = $item_data;
			}
			
			$this->db->free_result($query);
			
			return $items_list;
		}
		
		if($this->pn > 1)
		{
			header('location:'.SELF_LOCATION);
			exit;
		}
		
		return false;
	}
	
	/**
	 * Validates and saves task data into bdd
	 *
	 * @return string
	 */
	function Save()
	{
		if(isset($_POST['tdesc']) && $tdesc = $this->db->CleanString($_POST['tdesc']))
			$tdesc = $this->db->quote_smart($tdesc);
		else
			die(json_encode(array('message'=>'Descrierea taskului lipseste!','iserror'=>1)));
			
		if(isset($_POST['tdate']) && $tdate = self::DoDBDate($_POST['tdate']))
			$tdate = $this->db->quote_smart($tdate);
		else
			die(json_encode(array('message'=>'Data taskului lipseste!','iserror'=>1)));
		
		
		$this->db->do_query("INSERT INTO tasks (tdesc, tdate) VALUES ('$tdesc', '$tdate')");
		$this->id = $this->db->insert_id();
		if($this->id)
			die(json_encode(array('message'=>'Taskul a fost adaugat.', 'iserror'=>0)));
	}
	
	/**
	 * Executes sql query in order to obtain deleted tasks data from table tasks_log
	 *
	 * @return mixed
	 */
	function ViewLog()
	{
		
		$query = $this->db->do_query("SELECT SQL_CALC_FOUND_ROWS * FROM tasks_log ORDER BY tdelday DESC LIMIT ".self::getCurrentIndex().",".$this->items_per_page);
		
		if($query && mysql_num_rows($query))
		{
			$_query = $this->db->do_query("SELECT FOUND_ROWS() as total",true);
			$items_list['pages']['curpage']    = $this->pn;
			$items_list['pages']['totalpages'] =  ceil($_query['total']/$this->items_per_page);
			$items_list['pages']['prevpage']   = ($this->pn > 1)?($this->pn -1):null;
			$items_list['pages']['next']   	   = ($this->pn != $items_list['pages']['totalpages'])?($this->pn +1):null;
			
			while($item_data = mysql_fetch_row($query, MYSQL_ASSOC))
			{
				$item_data['tstatus_class'] = ($item_data['tstatus'])?'success':'info';
				$item_data['tstatusstr']    = ($item_data['tstatus'])?'Da':'Nu';
				$item_data['tdelday']    = date('d-m-Y',strtotime($item_data['tdelday']));
				$items_list['items'][] = $item_data;
			}
			
			$this->db->free_result($query);
			return $items_list;
		}
		
		return false;
	}
	/**
	 * Changes task status based on id
	 *
	 * @return string
	 */
	function SetStatus()
	{
		if($this->id)
		{
			$this->db->do_query("UPDATE tasks SET tstatus = 1 WHERE id = '{$this->id}'");
			die(json_encode(array('message'=>'Taskul a fost inchis.', 'iserror'=>0)));
		}
	}
	
	/**
	 * Executes query to replicate essetial task info into table tasks_log and deletes task entry from tasks based on task id
	 *
	 * @return string
	 */
	function Delete()
	{
		if($this->id)
		{
			$this->db->do_query("INSERT INTO tasks_log (tdesc, tstatus, tdelday) SELECT tdesc, tstatus, NOW() FROM tasks WHERE id = '{$this->id}'");
			$this->db->do_query("DELETE FROM tasks WHERE id = '{$this->id}'");
			die(json_encode(array('message'=>'Taskul a fost sters.', 'iserror'=>0)));
		}
	}
	
	function getCurrentIndex()
	{
		return (self::getCurrentPage()-1)*$this->items_per_page;
	}
	
	function getCurrentPage()
	{
		return $this->pn;
	}
}

?>