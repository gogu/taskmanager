<?php
/**
 * Database class
 *  @author     Alexandru Tertisco <alex@diffstudios.com>
 *  @version    1.0.1  (01.07.2008)
 *  @copyright  (c) 2008 - 2014 Alexandru Tertisco
 *  @package    TaskManager
 *
 **/
 
class Database
{
	/**
	* Connection to MySQL.
	*
	* @var string
	*/
	var $link;

	/**
	* Holds the most recent connection.
	*
	* @var string
	*/
	var $recent_link = null;

	/**
	* Holds the contents of the most recent SQL query.
	*
	* @var string
	*/
	var $sql = null;

	/**
	* The text of the most recent database error message.
	*
	* @var string
	*/
	var $error = null;

	/**
	* The error number of the most recent database error message.
	*
	* @var integer
	*/
	var $errno = null;

	/**
	* Constructor. Initializes a database connection and selects our database.
	*
	* @param  string  Database host
	* @param  string  Database username
	* @param  string  Database password
	* @param  string  Database name
	* @return boolean
	*/  
	
	function Database()
	{
        $required_constants = array('DBhost','DBuser','DBname');
		
		foreach($required_constants as $required_const)
		{
			if(!defined($required_const))
				exit($required_const.' isn\'t set!');
		}
		
		if(!$this->link)
			$this->link = @mysql_connect(DBhost, DBuser, DBpass);

        if ($this->link)
		{
            if (@mysql_select_db(DBname, $this->link))
			{
                $this->recent_link = $this->link;
                return $this->link;
            }
        }
	    // If we couldn't connect or select the db...
       die('Could not select and/or connect to database.');
    } 
	
	private function stripMagicQuotes($var)
	{
	    return  (get_magic_quotes_gpc())?stripslashes($var):$var;
	}
	
	/**
	 * Executes a sql query. If optional $only_first is set to true, it will
	 * return the first row of the result as an array.
	 *
	 * @param  string  Query to run
	 * @param  bool    Return only the first row, as an array?
	 * @return mixed
	 */
	 
    function do_query($sql, $only_first = false)
	{
	    $this->recent_link =$this->link;
        $this->sql =$sql;
        $result = @mysql_query($sql, $this->link);
		
        if ($only_first)
		{
            $return = $this->fetch_array($result);
            $this->free_result($result);
            return $return;
        }
        return $result;
    }
	
	/**
	* Fetches a row from a query result and returns the values from that row as an array.
	*
	* @param  string  The query result we are dealing with.
	* @return array
	*/
	function fetch_array($result){
		return @mysql_fetch_assoc($result);
	}

	/**
	* Returns the number of rows in a result set.
	*
	* @param  string  The query result we are dealing with.
	* @return integer
	*/
	function num_rows($result)
	{
		return @mysql_num_rows($result);
	}
	
	/**
	* Returns the ID of the most recently inserted item in an auto_increment field
	*
	* @return  integer
	*/
    function insert_id(){
        return @mysql_insert_id($this->link);
    } 
	
	/**
	* Escapes a value to make it safe for using in queries.
	*
	* @param  string  Value to be escaped
	* @param  bool    Do we need to escape this string for a LIKE statement?
	* @return string
	*/
    function quote_smart($value)
	{
        $value = $this->stripMagicQuotes($value);
        return (function_exists('mysql_real_escape_string'))?mysql_real_escape_string($value, $this->link):mysql_escape_string($value);
    }

    /**
	* Frees memory associated with a query result.
	*
	* @param  string   The query result we are dealing with.
	* @return boolean
	*/
	
    function free_result($result)
	{
        return @mysql_free_result($result);
    }

    /**
	* Closes our connection to MySQL.
	*
	* @param  none
	* @return boolean
	*/
    function close()
	{
        $this->sql = '';
        return @mysql_close($this->link);
    }

    /**
	* Returns the MySQL error message.
	*
	* @param  none
	* @return string
	*/
    function error()
	{
        $this->error = (is_null($this->recent_link)) ? '' : mysql_error($this->recent_link);
        return $this->error;
    }

    /**
	* Returns the MySQL error number.
	*
	* @param  none
	* @return string
	*/
    function errno()
	{
        $this->errno = (is_null($this->recent_link)) ? 0 : mysql_errno($this->recent_link);
        return $this->errno;
    } 
	
	/**
	* Returns a value stripped of HTML/JS code
	*
	* @param  string  Value to be cleaned
	* @return string
	*/
	function cleanstring($string2clean = null)
	{
		$string2clean = trim(stripslashes($string2clean));
		
		if($string2clean)
		{
			$search = array ("'<script[^>]*?>.*?</script>'si",
							 "'<a href[^>]*?>(.*?)<\\/a>'",
							 "'<img [^>]*? />'si",
							 "'<[\/\!]*?[^<>]*?>'si");
							 
			$string2clean = preg_replace($search, '', $string2clean);
			$string2clean = strip_tags($string2clean);
			return $string2clean;
		}
		return false;
	}
}
?>