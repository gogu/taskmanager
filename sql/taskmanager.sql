-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2014 at 02:37 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taskmanager`
--
CREATE DATABASE IF NOT EXISTS `taskmanager` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `taskmanager`;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tdesc` varchar(512) NOT NULL,
  `tdate` date NOT NULL,
  `tstatus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `tdesc`, `tdate`, `tstatus`) VALUES
(1, 'Collaborate on shared tasks', '2014-05-01', 1),
(2, 'Access tasks everywhere', '2014-05-02', 1),
(3, 'Beautiful design', '2014-05-02', 0),
(4, 'Get more features. Get more done.', '2014-05-02', 0),
(6, 'Apply filters to hide tasks and concentrate on what''s important.', '2014-05-03', 0),
(7, 'Analyze the details of your tasks to determine the best use of your limited time. ', '2014-05-04', 0),
(8, 'Alarms can arrive via email, sms, Twitter, iPhone or on your desktop.', '2014-05-03', 1),
(9, 'Use folders, tags, contexts, subtasks and more to organize, search and sort through your tasks.', '2014-05-03', 1),
(10, 'Easily work with other people on shared projects with collaboration tools.', '2014-05-04', 0),
(11, 'Get reminders for tasks that are due soon. .', '2014-05-04', 0),
(12, 'A smart list that automatically figures', '2014-05-11', 0),
(13, 'Concentrate on what''s important.', '2014-05-10', 1),
(14, 'Analyze the details of your tasks', '2014-05-04', 0),
(15, 'Alarms can arrive via email, sms, Twitter', '2014-05-03', 0),
(30, 'submit-ul sa fie request AJAX', '2014-05-14', 1),
(17, 'Work with other people on shared projects with collaboration tools.', '2014-05-13', 0),
(18, 'Get reminders for tasks.', '2014-05-13', 0),
(31, 'sa se poate vedea "status"-ul task-ului (daca este facut sau nu), data task-ului si descrierea', '2014-05-14', 1),
(29, 'sa nu se poata alege date din trecut', '2014-05-14', 0),
(28, 'formularul sa foloseasca calendar JS (preferabil jQuery UI dar se poate folosi orice)', '2014-05-14', 0),
(32, 'sa se poata pune un task ca si "facut" sau ca "nefacut"', '2014-05-14', 1),
(33, 'sa se poata sterge un task cu confirmare la stergere', '2014-05-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasks_log`
--

CREATE TABLE IF NOT EXISTS `tasks_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tdesc` varchar(255) NOT NULL,
  `tstatus` tinyint(1) unsigned NOT NULL,
  `tdelday` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tasks_log`
--

INSERT INTO `tasks_log` (`id`, `tdesc`, `tstatus`, `tdelday`) VALUES
(1, 'Task de test 7', 0, '2014-05-14 04:05:42'),
(2, 'Task de test 6', 0, '2014-05-14 04:05:44'),
(3, 'Task de test 5', 0, '2014-05-14 04:05:46'),
(4, 'Task de test 4', 0, '2014-05-14 04:05:48'),
(5, 'Task de test 3', 0, '2014-05-14 04:05:50'),
(6, 'Task de test 2', 0, '2014-05-14 04:05:51'),
(7, 'Task de test', 0, '2014-05-14 04:05:53'),
(8, 'Use folders, tags, contexts, subtasks and more to organize your tasks.', 1, '2014-05-14 04:14:02'),
(9, 'Task de test 8', 0, '2014-05-14 04:14:50'),
(10, 'A smart list that automatically figures out what''s important to you right now.', 0, '2014-05-14 04:17:52'),
(11, 'Task bla bla', 0, '2014-05-14 04:23:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
